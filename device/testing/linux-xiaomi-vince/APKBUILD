# Maintainer: m0rf30 <morf3089@gmail.com>
# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/vince_defconfig

pkgname=linux-xiaomi-vince
pkgver=4.9.236
pkgrel=1
pkgdesc="Xiaomi Redmi Note 5 Plus kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="xiaomi-vince"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="sed gcc-arm-none-eabi bash bc bison devicepkg-dev flex openssl-dev perl"

# Source
_repository="M0Rf30/kernel_xiaomi_vince"
_commit="1c360dbfc792d1ef2761a74036e6db0d83cfcba1"
_config="config-$_flavor.$arch"
source="$pkgname-$_commit.tar.gz::https://github.com/$_repository/archive/$_commit.tar.gz
	$_config
	"
builddir="$srcdir/kernel_xiaomi_vince-$_commit"
_outdir="out"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make O="$_outdir" ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1))-postmarketOS"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor" "$_outdir"
	# Modules
	unset LDFLAGS
	make O="$_outdir" ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1))-postmarketOS" \
		INSTALL_MOD_PATH="$pkgdir" modules_install
}

sha512sums="627301618b515b81cdeb3ef956129ddaacc68d34c8cf6da84b60f6924283d423f4251092613cf2450099996c2fa89fe3a4bfd1f752ae8d62b35f22c192dfe690  linux-xiaomi-vince-1c360dbfc792d1ef2761a74036e6db0d83cfcba1.tar.gz
95c12dff223bdea9d1bcc6aaab37a166967bfefd2874b8658465d1386d2ef61a5e9fd2f765f24d31b5a4c787a3136461205640f9f1232095ce8bc7fca244025b  config-xiaomi-vince.aarch64"
