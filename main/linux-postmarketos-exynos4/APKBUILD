# Contributor: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Maintainer: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Kernel config based on: arch/arm/configs/exynos_defconfig

pkgname=linux-postmarketos-exynos4
pkgver=5.9.1
pkgrel=0
pkgdesc="Mainline kernel fork for Samsung Exynos4 devices"
arch="armv7"
_carch="arm"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison busybox-static-$arch findutils flex installkernel openssl-dev perl xz"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v${_kernver%%.*}.x/linux-$_kernver.tar.xz
	$_config
	0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
	initramfs.list
	init
"
builddir="$srcdir/linux-$_kernver"

prepare_isorec() {
	# https://wiki.postmarketos.org/wiki/Boot_process#isorec
	cp -v /usr/$(arch_to_hostspec $arch)/bin/busybox.static \
		"$builddir"/usr/
	cp -v "$srcdir"/init "$builddir"/usr/
	cp -v "$srcdir"/initramfs.list "$builddir"/usr/
}

prepare() {
	default_prepare
	prepare_isorec
	cp -v "$srcdir/$_config" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="96af08c1dc964e5dbb3d221880ef8ca246f13370d2dc2f5ae64039f0352efc687c7446a2b4f86dad553b776ee75f78bbf58bb9bc845c9b57a9013ece63264874  linux-5.9.1.tar.xz
c817cbc005b7f3c14c2f171c800753b6180f3a72d8164db977c341015b331fe0865c4024454a22604765bc53f1df4554b29f3c8b86c3ab9f8020c8ba148960e7  config-postmarketos-exynos4.armv7
4bb111db4396a1981ad0883737c6594ca93701699846298b940d2ea202c666be0158a0f9ddc5b95b9147fa4cfe62639512ae78e9315f8d975b379ccc4e15da36  0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
aaff0332b90e1f9f62de1128cace934717336e54ab09de46477369fa808302482d97334e43a85ee8597c1bcab64d3484750103559fea2ce8cd51776156bf7591  initramfs.list
09f1f214a24300696809727a7b04378887c06ca6f40803ca51a12bf2176a360b2eb8632139d6a0722094e05cb2038bdb04018a1e3d33fc2697674552ade03bee  init"
